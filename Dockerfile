FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD /target/hello-word-rest-api.jar hello-word-rest-api.jar
ENTRYPOINT ["sh","-c","java -jar hello-word-rest-api.jar"]

